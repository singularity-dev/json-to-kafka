package json.to.kafka;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import json.to.kafka.reader.ReadJsonFiles;
import json.to.kafka.sender.ToKafkaSender;

public class Application {
	private final ReadJsonFiles readJsonFiles = new ReadJsonFiles();
	private final ToKafkaSender sendToKafka = new ToKafkaSender();

	public static void main(String[] args) throws Exception {
		Application application = new Application();
		application.run();
		System.out.println("All tasks were completed.");
	}

	private void run() throws Exception {
		List<String> strings = readJsonFiles.retrieveFileContents();
		strings.forEach(item -> {
			try {
				Thread.sleep(200);
				sendToKafka.send(item);
			} catch (InterruptedException | TimeoutException | ExecutionException e) {
				e.printStackTrace();
			}
		});
	}
}
