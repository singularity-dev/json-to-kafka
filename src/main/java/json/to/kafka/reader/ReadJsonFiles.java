package json.to.kafka.reader;

import static java.nio.file.Files.readAllLines;
import static java.util.logging.Level.FINER;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ReadJsonFiles {
	private static final Logger LOGGER = Logger.getLogger(ReadJsonFiles.class.getName());
	private static final String FILE_PATH = "json";

	public List<String> retrieveFileContents() throws IOException {
		List<String> content = new ArrayList<>();
		File directory = new File(FILE_PATH);

		if (directory.isDirectory()) {
			for (File file : directory.listFiles()) {
				LOGGER.log(FINER, file.getName());
				content.add(String.join("", readAllLines(Path.of(file.getPath()))).trim());
			}

			return content;
		}

		throw new IOException("Directory not found exception");
	}
}
