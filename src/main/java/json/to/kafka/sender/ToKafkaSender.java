package json.to.kafka.sender;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ToKafkaSender {
	private static final Gson gson = new Gson();
	private static final KafkaProducer<String, String> producer = new KafkaProducer<>(KafkaConfig.getProperties());

	public void send(String data) throws ExecutionException, InterruptedException, TimeoutException {
		JsonElement jsonElement = JsonParser.parseString(data);
		String topic = ((JsonObject) jsonElement).get("topic").getAsString();
		String content = gson.toJson(((JsonObject) jsonElement).get("content"));

		producer.send(new ProducerRecord<>(topic, content)).get(1500, TimeUnit.MILLISECONDS);
	}
}
